## Copyright (C) 2019 Juan Pablo Carbajal
## Copyright (C) 2005-2019 Søren Hauberg
## Copyright (C) 2010 VZLU Prague, a.s.
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @defun {@var{} =} pkg_installed_list ()
## @defunx {@var{} =} pkg_installed_list (@var{pkgnames})
##
## @seealso{}
## @end defun

function [pkglst, infostate] = pkg_installed_list (varargin)

  persistent local_list = tilde_expand (fullfile ("~", ".octave_packages"));
  persistent global_list = fullfile (OCTAVE_HOME (), "share", "octave",
                                     "octave_packages");

  pkgname = {};
  if (nargin > 0)
    pkgname = varargin;
  endif
  ## Get the list of installed packages.
  try
    local_packages = load (local_list).local_packages;
  catch
    local_packages = {};
  end_try_catch
  try
    global_packages = load (global_list).global_packages;
    global_packages = expand_rel_paths (global_packages);
    if (ispc)
      ## On Windows ensure 8.3 style paths are turned into LFN paths
      global_packages = standardize_paths (global_packages);
    endif
  catch
    global_packages = {};
  end_try_catch
  installed_pkgs_lst = {local_packages{:}, global_packages{:}};

  num_packages = numel (installed_pkgs_lst);
  if (num_packages == 0)
    infostate =  'no packages installed';
    pkglst    = {};
    return
  endif

  ## Eliminate duplicates in the installed package list.
  ## Locally installed packages take precedence.
  installed_names = cellfun (@(x) x.name, installed_pkgs_lst,
                             'uniformoutput', false);
  [~, idx] = unique (installed_names, 'first');
  installed_names = installed_names(idx);
  installed_pkgs_lst = installed_pkgs_lst(idx);

  ## Check whether info on a particular package was requested
  if (! isempty (pkgname))
    [~, idx]           = ismember (pkgname, installed_names);
    installed_names    = installed_names(idx);
    installed_pkgs_lst = installed_pkgs_lst(idx);
  endif

  num_packages = numel (installed_pkgs_lst);
  if (num_packages == 0)
    infostate = 'given package not installed';
    pkglst    = {};
    return
  endif

  if (num_packages < numel(pkgname))
    infostate = 'some given package not installed';
    pkglst = installed_pkgs_lst;
    return
  endif

  infostate = 'sucess';
  pkglst = installed_pkgs_lst;

endfunction
