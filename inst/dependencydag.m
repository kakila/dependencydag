## Copyright (C) 2019 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2019-12-18

## -*- texinfo -*-
## @defun {[@var{dag}, @var{V}, @var{E}] =} dependencydag ()
## @defunx {[@dots{}] =} dependencydag (@var{name}, @dots{})
##
##
## @end defun

function [depdag, nodes, edges] = dependencydag (varargin)

  # Organize dependencies of installed packages
  pkglst = pkg_installed_list ();

  nodes = cellfun (@(x) x.name, pkglst, 'unif', 0);
  depswithver = cellfun(@(x) x.depends, pkglst, 'unif', 0);
  deps = cellfun(@(x) cellfun(@(y) y.package, x, 'unif', 0)(:),
                                                depswithver, 'unif', 0);

  nnodes = numel (nodes);
  edges  = zeros (0, 2);
  for parent = 1:nnodes
    tf = ismember (nodes, deps{parent});
    if (any(tf))
      child  = find (tf);
      nchild = length (child);
      edges_ = [repmat(parent, nchild, 1) child(:)];
      edges  = vertcat (edges, edges_);
    endif
  endfor

  if (nargin > 0)
    pkgname = varargin;
    tf      = ismember (nodes, pkgname);
    pkgid   = find (tf);
  else
    pkgid = 1:nnodes;
  endif

  depdag = arrayfun (@(n)dag_struct(n, edges, nnodes), pkgid, 'unif', 0);

  if (nargin > 0)
    # bookkeeping in case only a subset of packages is requested

    # extract participating nodes and edges, global ids
    nodes_id = unique (cell2mat (cellfun (@(x) x.nodes, depdag, 'unif', 0)(:)));
    edges_id = unique (cell2mat (cellfun (@(x) x.edges, depdag, 'unif', 0)(:)));

    # renumber nodes and edges in local DAGs
    depdag = cellfun (@(g)dag_renumber(g, nodes_id, edges_id), ...
                                                             depdag, 'unif', 0);

    # renumber edges in global lists
    new_nnodes = length(nodes_id);
    tokeep     = false (size (edges, 1), 2);
    for i=1:new_nnodes
       tf        = edges == nodes_id(i);
       edges(tf) = i;
       tokeep   |= tf;
    endfor
    # if both nodes in an edge are kept, keep the whole edge
    edges = edges(all (tokeep, 2),:);

    nodes = nodes(nodes_id);
  endif

  if (length (pkgid) == 1)
    depdag = depdag{1};
  endif

endfunction

function [visited, walked, hascycle, order] = bfs_expand(s, E, nV)
  ONE     = ones(nV, 1);
  visited = tovisit = false (nV, 1);

  nE     = size (E, 1);
  walked = false (nE, 1);

  hascycle = false;

  order = zeros (nV, 1);

  tovisit(s) = true;
  while (any (tovisit))
    # All nodes to visited
    visited |= tovisit;
    order(tovisit) = max (order) + 1;

    # Set all children to be visisted in the next iteration
    tovisit_id = find (tovisit);
    # We will walk those edges. The union warrants each edge only once
    towalk = ismember (E(:,1), tovisit_id);
    if (any(ismember (E(towalk, 2), find (visited))))
      hascycle = true;
    endif
    walked |= towalk;

    for i=1:length(tovisit_id)
      towalk   = E(:,1) == tovisit_id(i);
      tovisit |= accumarray ([E(towalk, 2) ONE(towalk)], true, [nV, 1]);
    endfor
    tovisit &= !visited; # Remove all nodes that were already visisted
  endwhile
  order(!visited) = [];
endfunction

function dag = dag_struct(i, E, nV)
  [vstd, wlkd, cycl, order] = bfs_expand (i, E, nV);
  [order, o] = sort (order);
  dag = struct('nodes', find (vstd)(o), 'edges', find (wlkd),
                        'hascycle', cycl, 'order', order);
endfunction

function dag = dag_renumber(dag, node_id, edge_id)
  # Renumber DAG's edges and nodes using given nodes and edges as reference
  nn = length (node_id);
  for i = 1:nn
    dag.nodes(dag.nodes == node_id(i)) = i;
  endfor

  ne = length (edge_id);
  for i = 1:ne
    dag.edges(dag.edges == edge_id(i)) = i;
  endfor

endfunction

%!demo
%! [DAG, V, E] = dependencydag ();
%! nDAG = numel (DAG);
%!
%! figure (1);
%! clf ();
%! hold on;
%! nx   = floor (sqrt (nDAG));
%! dxdy = [0 0];
%! maxXY = [0 0];
%! counter = 0;
%! for id = 1:nDAG;
%!   v  = DAG{id}.nodes;
%!   nv = length(v);
%!   if (nv > 1)
%!     if (DAG{id}.hascycle)
%!       printf ('Circular dependencies found in: %s\n', V{id});
%!       continue
%!     endif
%!     counter +=1;
%!     e  = DAG{id}.edges;
%!     XY = zeros(nv,2);
%!     A  = zeros(nv);
%!     for i = 2:nv
%!       haspred = E(e,2) == v(i);
%!       pred    = find(ismember(v, E(e(haspred),1)));
%!       XY(i,1) = min(XY(pred,1)) + 1;
%!       A(pred, i) = 1;
%!     endfor
%!     XY(2:end,2) = mod(cumsum(XY(2:end,1)),XY(end,1));
%!
%!
%!     if (mod(counter, nx) == 0);
%!       dxdy(1) = 0;
%!       dxdy(2) = maxXY(2) + 1;
%!       maxXY(1) = 0;
%!     else
%!       dxdy(1) = maxXY(1) + 1;
%!     endif
%!     XY += dxdy;
%!     maxXY = max([maxXY; XY]);
%!
%!     gplot(A,XY,'-o');
%!     h = text (XY(:,1), XY(:,2)+0.1, V(v), 'interpreter', 'none');
%!     set (h(1), 'fontweight', 'bold');
%!     axis tight
%!   endif
%! endfor
%! box off; axis off

%!demo
%! [DAG, V, E] = dependencydag ('statistics', 'signal', 'generate_html', 'bim');
%! nDAG = numel (DAG);
%!
%! figure (1);
%! clf ();
%! hold on;
%! nx   = floor (sqrt (nDAG));
%! dxdy = [0 0];
%! maxXY = [0 0];
%! counter = 0;
%! for id = 1:nDAG;
%!   v  = DAG{id}.nodes;
%!   nv = length(v);
%!   if (nv > 1)
%!     if (DAG{id}.hascycle)
%!       printf ('Circular dependencies found in: %s\n', V{id});
%!       continue
%!     endif
%!     counter +=1;
%!     e  = DAG{id}.edges;
%!     XY = zeros(nv,2);
%!     A  = zeros(nv);
%!     for i = 2:nv
%!       haspred = E(e,2) == v(i);
%!       pred    = find(ismember(v, E(e(haspred),1)));
%!       XY(i,1) = min(XY(pred,1)) + 1;
%!       A(pred, i) = 1;
%!     endfor
%!     XY(2:end,2) = mod(cumsum(XY(2:end,1)),XY(end,1));
%!
%!
%!     if (mod(counter, nx) == 0);
%!       dxdy(1) = 0;
%!       dxdy(2) = maxXY(2) + 1;
%!       maxXY(1) = 0;
%!     else
%!       dxdy(1) = maxXY(1) + 1;
%!     endif
%!     XY += dxdy;
%!     maxXY = max([maxXY; XY]);
%!
%!     gplot(A,XY,'-o');
%!     h = text (XY(:,1), XY(:,2)+0.1, V(v), 'interpreter', 'none');
%!     set (h(1), 'fontweight', 'bold');
%!     axis tight
%!   endif
%! endfor
%! box off; axis off
