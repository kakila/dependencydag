## Copyright (C) 2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{str} =} dag2graphviz (@var{dag})
## @defunx {@var{str} =} dag2graphviz (@var{dag}, @var{V}, @var{E})
## Converts a localized DAG into a graphviz graph.
##
## The graphviz graph is returned as a string in @var{str}.
##
## If @var{V} and @var{E} are given, then @var{dag} nodes and edges contains
## indexes into @var{V} and @var{E}, respectively (i.e. the @var{dag} is not
## localized).
##
## @seealso{localizedag, dependencydag}
## @end defun

function str = dag2graphviz (dag, V={}, E=[])

  if(nargin > 1)
    if (nargin < 3)
      print_usage ();
    endif
    dag = localizedag (dag, V, E);
  endif

  if (iscell (dag))
    str = cellfun (@dag2graphviz, dag, 'unif', 0);
    return
  endif

  # Parse options
  fontsize = 12;

  body_str = sprintf (["## Created by GNU Octave %s\n" ...
  "## Command to get the layout: " ...
  "'dot -Tpng thisfile > thisfile.png'\n\n", ...
  "digraph G {\n\n## Configuration\n%%s}"], ...
                      version);
  g_config_str = sprintf (["graph [ " ...
                         'fontsize=%d\n' ...
                         'labelloc="t"\n' ...
                         'label=""\n' ...
                         'splines=true\n' ...
                         'overlap=false\n' ...
                         'rankdir="TB"\n'...
                         "];\n\n%%s"], ...
                         fontsize);
  body_str = sprintf (body_str, g_config_str);

  n_config_str = sprintf (["node [" ...
                         'style="filled,bold"\n' ...
                         'penwidth=1\n' ...
                         'fillcolor="white"\n' ...
                         'fontname="Courier New"\n' ...
                         'overlap=false\n' ...
                         'margin=0\n' ...
                         "];\n\n%%s" ...
                         ]);
  body_str = sprintf (body_str, n_config_str);

  e_config_str = sprintf (["edge [" ...
                         'penwidth=1\n' ...
                         'arrowType="normal"\n' ...
                         "];\n\n%%s" ...
                         ]);
  body_str = sprintf (body_str, e_config_str);

  body_str = sprintf (body_str, "ratio=compress;\n\n%s");

  # Node configuration
  body_str = sprintf (body_str, "## Nodes\n%s");
  nodelist = [sprintf('%s;\n', dag.nodes{:}) "\n%s"];
  body_str = sprintf (body_str, nodelist);
  # Edges structure
  if (length(dag.edges) > 0)
    body_str = sprintf (body_str, "## Edges\n%s");
    edgelist = [sprintf('%s -> %s\n', dag.edges.'{:}) "\n%s"];
    body_str = sprintf (body_str, edgelist);
  endif

  str = sprintf (body_str, "");

endfunction

%!test
%! [dag, V, E] = dependencydag();
%! ldag = localizedag(dag{1}, V, E);
%! lstr = dag2graphviz(ldag);
%! str = dag2graphviz(dag{1}, V, E);
%! assert (str, lstr)

%!test
%! [dag, V, E] = dependencydag();
%! cstr = dag2graphviz(dag, V, E);
%! str = dag2graphviz(dag{1}, V, E);
%! assert (cstr{1}, str)

%!demo
%! [dag, V, E] = dependencydag();
%! dag = localizedag(dag{1}, V, E);
%! str = dag2graphviz(dag)

%!demo
%! [dag, V, E] = dependencydag();
%! str = dag2graphviz(dag{1}, V, E)

%!demo
%! [dag, V, E] = dependencydag();
%! str = dag2graphviz(dag(1:3), V, E)
